#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

class Shader;

typedef struct TRAIL_VERTEX
{
	glm::vec4 edgePos;					// w coordinate is the direction from center (either -1 or 1)
	glm::vec3 forward;					// forward vector of trail at this edge
	float texOffset;					// texture coordinate offset along the y direction
	float originTime;					// what the parent Trail reference time was when this edge was created
	float warp;							// used for uneven trail growth for a more convincing effect
} TrailVertex;

class Trail
{
private:
	int maxEdges;						// how many edges we are allowed to have at once
	int numEdges;						// how many edges are currently active
	float edgeLife;						// how long each edge lasts, in seconds

	GLuint vao;
	GLuint vbo;							// we use just a single buffer to represent an array of TrailEdge objects

	Shader *shader;						// GLSL shader program

	GLuint diffuseMap;					// smoke texture we apply
	GLuint normalMap;					// smoke texture normals for lighting effect

	TrailVertex *vertices;				// positions and lives of vertices that make up the edges
	int maxVertices;					// how many vertices we're allowed (maxEdges / 2)
	int numVertices;					// how many we actually have in use

	float startWidth;					// how wide all edges are at their life beginning
	float endWidth;						// how wide all edges are at their life conclusion
	float widthPower;					// used to control the shape of the interpolation curve for width

	glm::vec4 startColor;				// colour of all edges at their life beginning
	glm::vec4 endColor;					// colour of all edges at their life conclusion
	float colorPower;					// used to control the shape of the interpolation curve for color
	float alphaPower;					// used to control the shape of the interpolation curve for alpha

	float scrollSpeed;					// controls how compressed along the y-axis the texture coordinates will be generated
	float referenceTexOffset;			// used to control smooth texture seams between trail segments
	float referenceTime;				// used to control the interpolation between start and end appearances in the shader
	bool updateEdges;					// flag for indicating the render() method needs to update the trail data on the GPU

	glm::mat4 viewProjection;			// necessary for proper billboarding
	glm::vec3 cameraPos;

	// initialization
	void setupVBOs();
	void setupShader();
	void setupTexture();

	void makeTrailEdge(TrailVertex *e1, TrailVertex *e2, const glm::vec3 &pos, const glm::vec3 &forward);

public:
	Trail(int maxEdges, float edgeLife, float scrollSpeed);
	~Trail();

	// configures the appearance of the smoke trail
	void setWidth(float startWidth, float endWidth, float widthPower);
	void setColor(const glm::vec4 &startColor, const glm::vec4 &endColor, float colorPower, float alphaPower);

	// clears all trails
	void reset();

	// returns true iff no trails are active, i.e., numEdges == 0
	bool isDead();

	// add a trail segment edge at the given coordinates
	void add(const glm::vec3 &pos, const glm::vec3 &forward);

	// the renderer needs to know the camera properties to billboard properly
	void prepareView(const glm::mat4 &viewProjection, const glm::vec3 &cameraPos);

	// advances the reference time
	void update(float dt);

	// render the entire trail
	void render();
};
