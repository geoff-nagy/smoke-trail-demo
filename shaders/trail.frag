#version 330 core

uniform sampler2D u_DiffuseMap;
uniform sampler2D u_NormalMap;

uniform vec3 u_AmbientColor;
uniform vec3 u_SunDirection;
uniform vec3 u_SunColor;

in vec3 v_Normal;
in vec2 v_TexCoord;
in vec4 v_Color;
in float v_EdgeFactor;

out vec4 f_FragColor;

void main()
{
	// read our diffuse colour and our normal map
	vec4 diffuseColor =  texture(u_DiffuseMap, v_TexCoord) * v_Color;
	vec3 normalColor = (texture(u_NormalMap, v_TexCoord).rgb - vec3(0.5, 0.5, 0.5)) * 0.5;

	// add our normal map offset to our billboard normal
	vec3 normal = normalize(v_Normal + normalColor);

	// compute lambertian lighting term
	float nDotL = pow(clamp(dot(normal, u_SunDirection), 0.0, 1.0), 1.0);//, 0.5);

	// add a bit of brightness around the edges
	nDotL = clamp(nDotL + pow(abs(v_EdgeFactor), 2.0), 0.0, 1.0);

	// apply colour and lighting, but keep original opacity where we can
	f_FragColor = vec4((u_AmbientColor * diffuseColor.rgb) + (diffuseColor.rgb * u_SunColor * nDotL), diffuseColor.a);
}
