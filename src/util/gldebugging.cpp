#include "util/gldebugging.h"
#include "util/log.h"

#include <GL/glew.h>

#include <assert.h>
#include <windows.h>

#include <sstream>
#include <iostream>
using namespace std;

// thanks to:
// https://blog.nobel-joergensen.com/2013/02/17/debugging-opengl-part-2-using-gldebugmessagecallback/

static void APIENTRY openglCallbackFunction(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	stringstream ss;

	ss << "---------------------OPENGL ERROR DETECTED------------" << endl;
	ss << "message: "<< message << endl;
	ss << "type: ";
    switch (type)
    {
		case GL_DEBUG_TYPE_ERROR:
			ss << "ERROR";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			ss << "DEPRECATED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			ss << "UNDEFINED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			ss << "PORTABILITY";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			ss << "PERFORMANCE";
			break;
		case GL_DEBUG_TYPE_OTHER:
			ss << "OTHER";
			break;
    }
	ss << endl;

	ss << "id: " << id << endl;
	ss << "severity: ";
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_LOW:
		ss << "LOW";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
		ss << "MEDIUM";
        break;
    case GL_DEBUG_SEVERITY_HIGH:
		ss << "HIGH";
        break;
    }
	ss << endl;
	LOG_INFO(ss.str());
}

void initGLDebugger()
{
	GLuint unusedIds = 0;

	// make sure message callback debugging is supported
	if(glDebugMessageCallback)
	{
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(openglCallbackFunction, NULL);
        glDebugMessageControl(GL_DONT_CARE,
							  GL_DONT_CARE,
							  GL_DONT_CARE,
							  0,
							  &unusedIds,
							  true);
    }
    else
    {
        LOG_INFO("-- GL callback debugging is not available");
	}
}
