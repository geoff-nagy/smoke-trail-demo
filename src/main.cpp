// Smoke Trail Demo
// Geoff Nagy
// Demonstrates how to render lit smoke trails. The effect is certainly not physically realistic, but
// looks physically plausible. Use the mouse (click-and-drag) to look around, and the mouse wheel to
// zoom in and out.

// NOTE: special thanks to:
// 		http://codeflow.org/entries/2012/aug/05/webgl-rendering-of-solid-trails/
// for providing initial inspiration on the general technique

#include "rendering/grid.h"							// the grid floor reference
#include "rendering/trail.h"						// our smoke trail implementation

#include "util/gldebugging.h"						// GL callback debugging is absolutely essential, in my opinion
#include "util/log.h"								// useful for tracking if anything does go wrong

#include "gl/glew.h"								// OpenGL extension wrangler, otherwise we only get GL 1.1 or so
#include "glfw/glfw3.h"								// context, window, and input handling
#include "glm/glm.hpp"								// OpenGL Mathematics library to handle our vector and matrix math
#include "glm/gtc/random.hpp"						// for moving smoke source around randomly
#include "glm/gtc/noise.hpp"
#include "glm/gtc/matrix_transform.hpp"				// for lookAt() and perspective(), since we shouldn't use GLU for these (GLU is deprecated)
#include "glm/gtx/rotate_vector.hpp"				// used to compute camera viewing angles when rotating around the center of the world
using namespace glm;

#include <cstdlib>
#include <iostream>
using namespace std;

// GLFW window and characteristics
GLFWwindow *window;
vec2 windowSize;

// camera position
float cameraDistance, cameraAngleX, cameraAngleY;
vec3 cameraPos;

// track old mouse position for mouse control of camera
double oldMouseX, oldMouseY;

// properties of the orthographic view that we use for rendering the lasers
vec4 viewPort;
mat4 orthoProjection;
mat4 orthoView;

// properties of the perspective view that we use for rendering everything else
mat4 perspectiveProjection;
mat4 model;
mat4 perspectiveView;

// we have an invisible moving source of smoke to show off the effect
vec3 sourcePos = vec3(0.0f, 0.0f, -7.0f);
vec3 sourceVel = vec3(4.0f, 0.0f, 0.0f);

// intialization functions
void openWindow();
void prepareOpenGL();
void prepareViewingParams();

// camera control
void mouseScrollCallback(GLFWwindow* window, double x, double y);
void moveCameraWithMouse();
void setCameraPosition(float distance, float angleX, float angleY);

int main(int args, char *argv[])
{
	const float TARGET_FRAMERATE = 1.0f / 60.0f;

	const int TRAIL_MAX_EDGES = 500;							// each trail allocates enough vertices for 500 edges
	const vec4 TRAIL_START_COLOR(0.8f, 0.8f, 0.8f, 1.0f);		// trails begin with this colour
	const vec4 TRAIL_END_COLOR(0.7f, 0.7f, 0.7f, 0.0f);			// trails end with this colour

	// properties of longer, thinner trail
	const float THIN_TRAIL_LIFE = 8.0f;							// edge life in seconds
	const float THIN_TRAIL_SCROLL_SPEED = 0.0025f;				// how far to advance each tex coord per new edge
	const float THIN_TRAIL_START_WIDTH = 0.1f;					// spawned edges begin at this thickness; units arbitrary
	const float THIN_TRAIL_END_WIDTH = 1.0f;					// edges reach this thickness before expiring
	const float THIN_TRAIL_WIDTH_POWER = 0.75f;					// used to control curve of width interpolation
	const float THIN_TRAIL_COLOR_POWER = 1.0f;					// used to control curve of colour interpolation (alpha is separate)
	const float THIN_TRAIL_ALPHA_POWER = 10.0f;					// used to control curve of alpha interpolation

	// properties of shorter, lighter "wake"
	const float WIDE_TRAIL_LIFE = 1.0f;							// (analogous to params used in code block above, except
	const float WIDE_TRAIL_SCROLL_SPEED = 0.002f;				//  for the wide trail this time)
	const float WIDE_TRAIL_START_WIDTH = 0.1f;
	const float WIDE_TRAIL_END_WIDTH = 2.0f;
	const float WIDE_TRAIL_WIDTH_POWER = 1.5f;
	const float WIDE_TRAIL_COLOR_POWER = 1.0f;
	const float WIDE_TRAIL_ALPHA_POWER = 1.5f;

	// position and orientation of grid
	mat4 gridPos = translate(mat4(1.0f), vec3(0.0f, -4.0f, 0.0f));

	// our objects to render
	Grid *grid;								// floor grid for reference
	Trail *trail1;							// our smoke trail objects
	Trail *trail2;

	// controls trail looping
	float loopRotate = 0.01f;
	float loopCos = cos(loopRotate);
	float loopSin = sin(loopRotate);
	float temp;

	// open log file---useful for debugging OpenGL issues or dumb things I might do by accident (or on purpose)
	Log::open();
	LOG_INFO("logging begins");

	// create our OpenGL window and context, and set some rendering options
	openWindow();
	prepareOpenGL();
	prepareViewingParams();

	// initialize our graphical objects
	grid = new Grid(30, 1.0);

	// we have two trails: first is a thick, wispy layer of smoke
	trail1 = new Trail(TRAIL_MAX_EDGES, WIDE_TRAIL_LIFE, WIDE_TRAIL_SCROLL_SPEED);
	trail1 -> setWidth(WIDE_TRAIL_START_WIDTH, WIDE_TRAIL_END_WIDTH, WIDE_TRAIL_WIDTH_POWER);
	trail1 -> setColor(TRAIL_START_COLOR, TRAIL_END_COLOR, WIDE_TRAIL_COLOR_POWER, WIDE_TRAIL_ALPHA_POWER);

	// second trail is a thicker, thinner version of the first
	trail2 = new Trail(TRAIL_MAX_EDGES, THIN_TRAIL_LIFE, THIN_TRAIL_SCROLL_SPEED);
	trail2 -> setWidth(THIN_TRAIL_START_WIDTH, THIN_TRAIL_END_WIDTH, THIN_TRAIL_WIDTH_POWER);
	trail2 -> setColor(TRAIL_START_COLOR, TRAIL_END_COLOR, THIN_TRAIL_COLOR_POWER, THIN_TRAIL_ALPHA_POWER);

	// notify the user that all initialization has been successful
	LOG_INFO("starting main loop...");

	// we exit if the users presses the 'X' or the ESC key
	while(!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE))
	{
		// clear our colour buffer; there's no need to clear the depth buffer since we don't use it
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// update the camera position from mouse input
		moveCameraWithMouse();
		setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);

		// move the invisible source of smoke
		sourcePos += sourceVel * TARGET_FRAMERATE;

		// move the source in a big loop
		temp = sourceVel.x;
		sourceVel.x = loopCos * sourceVel.x - loopSin * sourceVel.z;
		sourceVel.z = loopSin * temp + loopCos * sourceVel.z;

		// emit smoke
		trail1 -> add(sourcePos, normalize(sourceVel));
		trail2 -> add(sourcePos, normalize(sourceVel));

		// render the grid floor first
		grid -> render(perspectiveProjection, gridPos, perspectiveView);

		// update and render our smoke trail
		trail1 -> prepareView(perspectiveProjection * perspectiveView, cameraPos);
		trail2 -> prepareView(perspectiveProjection * perspectiveView, cameraPos);
		trail1 -> update(TARGET_FRAMERATE);
		trail2 -> update(TARGET_FRAMERATE);
		trail1 -> render();
		trail2 -> render();

		// get input events and update our framebuffer
		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	// ready to exit
	LOG_INFO("exited main loop");

	// shut down our graphical objects and return the memory to the system
	delete trail2;
	delete trail1;
	delete grid;

	// shut down GLFW
	LOG_INFO("shutting down GLFW...");
	glfwDestroyWindow(window);
	glfwTerminate();

	// close log file
	LOG_INFO("logging ends");
	Log::close();
}

void openWindow()
{
	const int WIDTH = 1280;
	const int HEIGHT = 720;
	const char *TITLE = "Smoke Trail Rendering Demo";
	GLenum error;

	// we need to intialize GLFW before we create a GLFW window
	LOG_INFO("starting GLFW...");
	if(!glfwInit())
	{
		LOG_ERROR("openWindow() could not initialize GLFW");
	}

	// explicitly set our OpenGL context to something that doesn't support any old-school shit
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_REFRESH_RATE, 60);

	// create our OpenGL window using GLFW
	LOG_INFO("opening window...");
    window = glfwCreateWindow(WIDTH, HEIGHT,					// specify width and height
							  TITLE,							// title of window
							#ifdef DEBUG
							  NULL,								// always windowed if in debug mode
							#else
							  glfwGetPrimaryMonitor(),			// otherwise always full screen
							#endif
							  NULL);							// not sharing resources across monitors
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	// configure our viewing area
	glViewport(0, 0, WIDTH, HEIGHT);

	// enable our extensions handler
	LOG_INFO("starting GLEW...");
	glewExperimental = true;		// GLEW bug: glewInit() doesn't get all extensions, so we have it explicitly search for everything it can find
	error = glewInit();
	if(error != GLEW_OK)
	{
		LOG_ERROR(glewGetErrorString(error));
	}

	// clear the OpenGL error code that results from initializing GLEW
	glGetError();

	// save our window size for later on
	windowSize = vec2(WIDTH, HEIGHT);

	// print our OpenGL version info
    LOG_INFO("-- GL version:   " << glGetString(GL_VERSION));
	LOG_INFO("-- GL vendor:    " << glGetString(GL_VENDOR));
	LOG_INFO("-- GL renderer:  " << glGetString(GL_RENDERER));
	LOG_INFO("-- GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION));

	// OpenGL callback debugging is extremely useful, so enable it if we're in debug mode
	#ifdef DEBUG
		initGLDebugger();
		LOG_INFO( "-- GL callback debugging is enabled");
	#else
		LOG_INFO("-- GL callback debugging is disabled in non-Debug configurations");
	#endif
}

void prepareOpenGL()
{
	// turn off depth testing and enable blending
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	glPolygonMode(GL_FRONT, GL_FILL);

	// sky-ish background
	glClearColor(0.584f, 0.784f, 0.847f, 1.0);
}

void prepareViewingParams()
{
	const float FOV = 45.0f;
	const float ASPECT_RATIO = (float)windowSize.x / (float)windowSize.y;

	const float PERSPECTIVE_NEAR_RANGE = 0.01f;
	const float PERSPECTIVE_FAR_RANGE = 5000.0f;

	const float ORTHO_NEAR_RANGE = -1.0;
	const float ORTHO_FAR_RANGE = 1.0;

	const float STARTING_CAMERA_DISTANCE = 20.0f;
	const float STARTING_CAMERA_ANGLE_X = -M_PI / 4.0;
	const float STARTING_CAMERA_ANGLE_Y = 0.0f;

	const float HALF_WINDOW_WIDTH = windowSize.x / 2.0f;
	const float HALF_WINDOW_HEIGHT = windowSize.y / 2.0f;

	// compute our perspective projection---this never changes so we can just do it once
	perspectiveProjection = perspective(FOV, ASPECT_RATIO, PERSPECTIVE_NEAR_RANGE, PERSPECTIVE_FAR_RANGE);

	// set up our mouse
	glfwSetScrollCallback(window, mouseScrollCallback);

	// position our camera somewhere sane to start our with
	cameraDistance = STARTING_CAMERA_DISTANCE;
	cameraAngleX = STARTING_CAMERA_ANGLE_X;
	cameraAngleY = STARTING_CAMERA_ANGLE_Y;
	setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);

	// create an orthographic projection for later on; this never changes
	orthoProjection = ortho(-HALF_WINDOW_WIDTH, HALF_WINDOW_WIDTH, -HALF_WINDOW_HEIGHT, HALF_WINDOW_HEIGHT, ORTHO_NEAR_RANGE, ORTHO_FAR_RANGE);
	orthoView = lookAt(vec3(0.0, 0.0, 0.0), vec3(0.0, 0.0, -5.0), vec3(0.0, 1.0, 0.0));
	viewPort = vec4(-HALF_WINDOW_WIDTH, -HALF_WINDOW_HEIGHT, windowSize.x, windowSize.y);
}

void mouseScrollCallback(GLFWwindow* window, double x, double y)
{
	const float SCROLL_SPEED = 2.0;				// meters we scroll per scroll notch
	const float MIN_DIST = 4.0;					// how close we're allowed to get
	const float MAX_DIST = 40.0;				// how far we're allowed to get

	// adjust the camera distance either forwards or backwards, and keep it within a reasonable distance
	cameraDistance -= (y > 0.0 ? SCROLL_SPEED : -SCROLL_SPEED);
	if(cameraDistance > MAX_DIST) cameraDistance = MAX_DIST;
	if(cameraDistance < MIN_DIST) cameraDistance = MIN_DIST;
}

void moveCameraWithMouse()
{
	const float MAX_ANGLE = (M_PI / 2.0f) - 0.001;
	double mouseX, mouseY;

	// always update our mouse position
	glfwGetCursorPos(window, &mouseX, &mouseY);

	// only do something if we detect a mouse button press
	if(glfwGetMouseButton(window, 0))
	{
		cameraAngleX -= (mouseY - oldMouseY) * 0.01;
		cameraAngleY -= (mouseX - oldMouseX) * 0.01;
		if(cameraAngleX > MAX_ANGLE) cameraAngleX = MAX_ANGLE;
		if(cameraAngleX < -MAX_ANGLE) cameraAngleX = -MAX_ANGLE;
	}

	// track our old mouse position for mouse movement calculations next frame
	oldMouseX = mouseX;
	oldMouseY = mouseY;
}

void setCameraPosition(float distance, float angleX, float angleY)
{
	const vec3 CAMERA_LOOK(0.0f, 0.0f, 0.0f);	// look at center position
	const vec3 CAMERA_UP(0.0, 1.0, 0.0);		// define what direction the camera considers to be 'up'

	// position our camera around the center point based on the incoming distance and viewing angle
	cameraPos = vec3(0.0, 0.0, cameraDistance);
	cameraPos = rotate(cameraPos, angleX, vec3(1.0, 0.0, 0.0));
	cameraPos = rotate(cameraPos, angleY, vec3(0.0, 1.0, 0.0));

	// compute a matrix describing the position and orientation of our camera
	perspectiveView = lookAt(cameraPos, CAMERA_LOOK, CAMERA_UP);
}
