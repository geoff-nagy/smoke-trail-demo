#pragma once

#include "gl/glew.h"
#include "glm/glm.hpp"

class Shader;

class Grid
{
private:
	int lineCount;

	GLuint vao;
	GLuint vbos[1];

	Shader *shader;

	void setupVBOs(int lineCount, float lineSpacing);
	void setupShader();

public:
	Grid(int lineCount, float lineSpacing);
	~Grid();

	void render(glm::mat4 &projection, glm::mat4 &model, glm::mat4 &view);
};
