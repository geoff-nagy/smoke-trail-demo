#include "rendering/trail.h"

#include "util/loadtexture.h"
#include "util/shader.h"
#include "util/log.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/noise.hpp"
using namespace glm;

Trail::Trail(int maxEdges, float edgeLife, float scrollSpeed)
{
	this -> maxEdges = maxEdges;
	this -> edgeLife = edgeLife;
	this -> scrollSpeed = scrollSpeed;

	maxVertices = maxEdges * 2;
	vertices = new TrailVertex[maxVertices];

	setupVBOs();
	setupShader();
	setupTexture();

	reset();
}

Trail::~Trail()
{
	// delete trail vertex data
	delete[] vertices;

	// free GL memory
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);

	// done with shader
	shader -> unbind();
	delete shader;
}

void Trail::setupVBOs()
{
	// set up our VAO and generate our VBOs
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);

	// we use just one buffer for efficiency, and this is also convenient in terms of just sending an array
	// of TrailEdge objects to the GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TrailVertex) * maxVertices, NULL, GL_DYNAMIC_DRAW);

	// first property in the TrailEdge struct is the edge center position
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(TrailVertex), NULL);
	glEnableVertexAttribArray(0);

	// next property in the TrailEdge struct is the forward vector used to compute billboard direction
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(TrailVertex), (GLvoid*)sizeof(vec4));
	glEnableVertexAttribArray(1);

	// next property in the TrailEdge struct is the tex y-coordinate
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(TrailVertex), (GLvoid*)(sizeof(vec4) + sizeof(vec3)));
	glEnableVertexAttribArray(2);

	// next property in the TrailEdge struct is the reference time used for interpolating between start and end appearance
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(TrailVertex), (GLvoid*)(sizeof(vec4) + sizeof(vec3) + sizeof(GLfloat)));
	glEnableVertexAttribArray(3);

	// last property in the TrailEdge struct is the reference time used for interpolating between start and end appearance
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, sizeof(TrailVertex), (GLvoid*)(sizeof(vec4) + sizeof(vec3) + sizeof(GLfloat) + sizeof(GLfloat)));
	glEnableVertexAttribArray(4);
}

void Trail::setupShader()
{
	shader = new Shader("shaders/trail.vert", "shaders/trail.frag");
	shader -> bindAttrib("a_EdgePos", 0);
	shader -> bindAttrib("a_Forward", 1);
	shader -> bindAttrib("a_TexOffset", 2);
	shader -> bindAttrib("a_OriginTime", 3);
	shader -> bindAttrib("a_Warp", 4);
	shader -> link();
	shader -> bind();
	shader -> uniform1i("u_DiffuseMap", 0);
	shader -> uniform1i("u_NormalMap", 1);
	shader -> uniform1f("u_TextureSmokeWidth", 0.05f);						// may vary depending on texture
	shader -> uniform1f("u_EdgeLife", edgeLife);
	shader -> uniformVec3("u_AmbientColor", vec3(0.584f, 0.784f, 0.847f));	// sky color: may varying depending on world/scene settings
	shader -> uniformVec3("u_SunDirection", vec3(0.0f, 1.0f, 0.0f));		// sun dir: may varying depending on world/scene settings
	shader -> uniformVec3("u_SunColor", vec3(1.0f, 1.0f, 0.984f));			// may varying depending on world/scene settings
	shader -> unbind();
}

void Trail::setWidth(float startWidth, float endWidth, float widthPower)
{
	this -> startWidth = startWidth;
	this -> endWidth = endWidth;
	this -> widthPower = widthPower;
}

void Trail::setColor(const vec4 &startColor, const vec4 &endColor, float colorPower, float alphaPower)
{
	this -> startColor = startColor;
	this -> endColor = endColor;
	this -> colorPower = colorPower;
	this -> alphaPower = alphaPower;
}

void Trail::setupTexture()
{
	diffuseMap = loadTexture("png/smoke-diffuse.png");
	normalMap = loadTexture("png/smoke-normals.png");
}

void Trail::reset()
{
	numEdges = 0;
	numVertices = 0;
	referenceTime = 0.0f;
	referenceTexOffset = linearRand(0.0f, 1.0f);
}

bool Trail::isDead()
{
	return numEdges == 0;
}

void Trail::add(const vec3 &pos, const vec3 &direction)
{
	// if we have enough free space, shift all vertices down by 2; otherwise, if we don't have enough free space,
	// then we need to trash the one at the tail of the list by copying over it
	int verticesToMove = (numEdges < maxEdges ? numVertices : numVertices - 2);

	// update edge vertex edge count
	if(numVertices < maxVertices) numVertices += 2;
	if(numEdges < maxEdges) numEdges ++;

	// we need to make room for the new edge
	memmove(vertices + 2, vertices, sizeof(TrailVertex) * verticesToMove);

	// now insert the new edge in the space we just created
	makeTrailEdge(vertices, vertices + 1, pos, direction);

	// annoyingly, we must now inform the GPU of our shifted edges---I really wish OpenGL supported circular vertex buffers!
	// (the reason we don't update in add() is because we want to keep all OpenGL-related processing to the render() method;
	// this is useful in multithreading contexts since GL calls can only take place in the main thread)
	updateEdges = true;
}

void Trail::prepareView(const mat4 &viewProjection, const vec3 &cameraPos)
{
	this -> viewProjection = viewProjection;
	this -> cameraPos = cameraPos;
}

void Trail::update(float dt)
{
	// advance the reference timer; notice that we don't actually reset it to 0 or anything...this means that if the trail
	// were to run for a very, very long time, eventually we'd lose precision and things would not appear correct; I'm
	// operating under the assumption that trails are relatively short-lived, but if that were not the case, we could simply
	// just reset the reference time, and those of all other trails, if referenceTime here became too large
	referenceTime += dt;

	// decrease our edge/vertex count while a tail-most edge is dead; ideally, this should loop at most one time per Trail object
	// per update() call, assuming we make that call often (and we should!)
	while(numEdges && (referenceTime - vertices[numVertices - 1].originTime > edgeLife))
	{
		// forget about the last two vertices---they form a single edge that has died; note that we don't need to inform the
		// GPU of this change, since the deleted edge will now not be rendered anyways (since we decrease numVertices)
		numEdges --;
		numVertices -= 2;
	}
}

void Trail::render()
{
	// bind our vertex array and send updated trail data to the GPU if requested
	glBindVertexArray(vao);
	if(updateEdges)
	{
		// don't do this again
		updateEdges = false;

		// send updated trail vertex information to the GPU
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(TrailVertex) * numVertices, vertices);
	}

	// bind our shader and inform it of our reference time and camera perspective
	shader -> bind();
	shader -> uniform1f("u_ReferenceTime", referenceTime);
	shader -> uniformMatrix4fv("u_ViewProjection", 1, (GLfloat*)value_ptr(viewProjection));
	shader -> uniformVec3("u_CameraPos", cameraPos);

	// now, a note about uniforms:
	// notice that we're sending these uniforms every time we call render()---this isn't strictly required since
	// each instance of Trail will have its own copy of the trail shader, so you could change this quite easily;
	// I kept it this way since it is closer to what I require for my own projects, in which objects will sometimes
	// share shader instances between them

	// set width values
	shader -> uniform1f("u_StartWidth", startWidth);
	shader -> uniform1f("u_EndWidth", endWidth);
	shader -> uniform1f("u_WidthPower", widthPower);

	// set color values
	shader -> uniformVec4("u_StartColor", startColor);
	shader -> uniformVec4("u_EndColor", endColor);
	shader -> uniform1f("u_ColorPower", colorPower);
	shader -> uniform1f("u_AlphaPower", alphaPower);

	// bind the smoke textures we're going to use
	glBindTexture(GL_TEXTURE_2D, diffuseMap);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalMap);
	glActiveTexture(GL_TEXTURE0);

	// finally, render the geometry---the shader handles our billboarding for us
	glDrawArrays(GL_TRIANGLE_STRIP, 0, numVertices);
}

void Trail::makeTrailEdge(TrailVertex *e1, TrailVertex *e2, const vec3 &pos, const vec3 &forward)
{
	// advance to the next texture coordinate position so we can keep the smoke trails looking smooth from edge to edge
	referenceTexOffset += scrollSpeed;

	// vertex on one side of the edge
	e1 -> edgePos = vec4(pos, -1.0f);
	e1 -> forward = forward;
	e1 -> texOffset = referenceTexOffset;
	e1 -> originTime = referenceTime;
	e1 -> warp = 1.0f + pow(length(perlin(pos)), 1.0f) * 0.4f;				// add random smooth offset

	// vertex on the other side of the edge
	e2 -> edgePos = vec4(pos, 1.0f);
	e2 -> forward = forward;
	e2 -> texOffset = referenceTexOffset;
	e2 -> originTime = referenceTime;
	e2 -> warp = 1.0f + pow(length(perlin(-pos)), 1.0f) * 0.4f;				// add random smooth offset
}
