#pragma once

#include "gl/glew.h"

// returns a GL texture ID associated with the data in the named image file
GLuint loadTexture(const char *name);
