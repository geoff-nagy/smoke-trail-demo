#include "util/loadtexture.h"

#include "soil/soil.h"

#include "gl/glew.h"

#include <stdio.h>
#include <stdlib.h>

GLuint loadTexture(const char *name)
{
    int width;
    int height;
    unsigned char *data;
    GLuint result = 0;

	// faster than using SOIL_load_OGL_texture(), plus this doesn't use any deprecated functionality
	data = SOIL_load_image(name, &width, &height, 0, SOIL_LOAD_RGBA);

	// make sure the load was successful
	if(data)
	{
		// we can generate a texture object since we had a successful load
		glGenTextures(1, &result);
		glBindTexture(GL_TEXTURE_2D, result);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

		// texture UVs should not wrap
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// generate mipmaps for this texture
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		// release the memory used to perform the loading
		SOIL_free_image_data(data);
	}
	else
	{
		fprintf(stderr, "loadTexture() could not load\"%s\"\n", name);
		exit(1);
	}

    return result;
}
