#version 330 core

// general smoke trail properties
uniform float u_TextureSmokeWidth;
uniform float u_EdgeLife;

// current reference time to control this edge vertex's interpolation
uniform float u_ReferenceTime;

// edge width settings
uniform float u_StartWidth;
uniform float u_EndWidth;
uniform float u_WidthPower;

// edge colour settings
uniform vec4 u_StartColor;
uniform vec4 u_EndColor;
uniform float u_ColorPower;
uniform float u_AlphaPower;

// current camera view properties
uniform mat4 u_ViewProjection;
uniform vec3 u_CameraPos;

layout(location = 0) in vec4 a_EdgePos;
layout(location = 1) in vec3 a_Forward;
layout(location = 2) in float a_TexOffset;
layout(location = 3) in float a_OriginTime;
layout(location = 4) in float a_Warp;

out vec3 v_Normal;
out vec2 v_TexCoord;
out vec4 v_Color;
out float v_EdgeFactor;

void main()
{
	// compute how to roll to face the camera, as well as our billboard normal direction for lighting
	vec3 relative = normalize(u_CameraPos - a_EdgePos.xyz);
	vec3 side = normalize(cross(a_Forward, relative));
	vec3 up = normalize(cross(relative, side));
	v_Normal = (side * a_EdgePos.w) + cross(side, up);

	// compute how far along its life the vertex is
	float lifeFactor = clamp((u_ReferenceTime - a_OriginTime) / u_EdgeLife, 0.0, 1.0);
	float widthLifeFactor = pow(lifeFactor, u_WidthPower);
	float colorLifeFactor = pow(lifeFactor, u_ColorPower);
	float alphaLifeFactor = pow(lifeFactor, u_AlphaPower);

	// interpolate between start and end appearance based on our life factor
	float width = u_StartWidth + (u_EndWidth - u_StartWidth) * widthLifeFactor;
	v_Color.rgb = u_StartColor.rgb + (u_EndColor.rgb - u_StartColor.rgb) * colorLifeFactor;
	v_Color.a = u_StartColor.a + (u_EndColor.a - u_StartColor.a) * alphaLifeFactor;

	// add a small alpha component to the front of the trail
	v_Color.a *= pow(clamp(lifeFactor, 0.0, 0.05) / 0.05, 2.0);

	// soften weirdness at direct angles
	//vec3 vecToCamera = normalize(u_CameraPos - vec3(a_EdgePos));
	//v_Color.a *= pow(1.0 - abs(dot(a_Forward, vecToCamera)), 0.5);

	// compute vertex position for this edge
	vec3 vertexPos = a_EdgePos.xyz + (side * width * a_EdgePos.w * a_Warp);

	// inform the fragment shader of how close to the edge it is
	v_EdgeFactor = a_EdgePos.w;

	// compute texture coords based on whether we're the left or right vertex
	v_TexCoord = vec2(0.5f + (a_EdgePos.w * u_TextureSmokeWidth), a_TexOffset);

	// assign transformed position
	gl_Position = u_ViewProjection * vec4(vertexPos, 1.0);
}
