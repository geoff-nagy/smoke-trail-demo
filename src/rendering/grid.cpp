#include "rendering/grid.h"

#include "util/shader.h"

#include "gl/glew.h"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

Grid::Grid(int lineCount, float lineSpacing)
{
	this -> lineCount = lineCount;

	setupVBOs(lineCount, lineSpacing);
	setupShader();
}

Grid::~Grid()
{
	delete shader;
	glDeleteBuffers(1, vbos);
	glDeleteVertexArrays(1, &vao);
}

void Grid::setupVBOs(int lineCount, float lineSpacing)
{
	GLfloat vertices[lineCount * 12];
	GLfloat *vertex = vertices;
	int i;

	// add a horizontal (depending on your orientation) row of lines
	for(i = 0; i < lineCount; i ++)
	{
		*vertex++ = -((lineCount - 1) * lineSpacing) / 2.0;
		*vertex++ = 0.0;
		*vertex++ = -(((lineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;

		*vertex++ = ((lineCount - 1) * lineSpacing) / 2.0;
		*vertex++ = 0.0;
		*vertex++ = -(((lineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;
	}

	// add a vertical (depending on your orientation) row of lines
	for(i = 0; i < lineCount; i ++)
	{
		*vertex++ = -(((lineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;
		*vertex++ = 0.0;
		*vertex++ = -((lineCount - 1) * lineSpacing) / 2.0;

		*vertex++ = -(((lineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;
		*vertex++ = 0.0;
		*vertex++ = ((lineCount - 1) * lineSpacing) / 2.0;
	}

	// set up our VAO and VBOs
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, vbos);

	// the only vertex attributes we have to define are line end point positions, and these never change
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * lineCount * 12, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
}

void Grid::setupShader()
{
	shader = new Shader("shaders/line.vert", "shaders/line.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> link();
	shader -> bind();
	shader -> uniformVec4("u_Color", vec4(0.2, 0.2, 0.2, 0.2));
	shader -> unbind();
}

void Grid::render(mat4 &projection, mat4 &model, mat4 &view)
{
	shader -> bind();
	shader -> uniformMatrix4fv("u_Projection", 1, value_ptr(projection));
	shader -> uniformMatrix4fv("u_Model", 1, value_ptr(model));
	shader -> uniformMatrix4fv("u_View", 1, value_ptr(view));

	glBindVertexArray(vao);
	glDrawArrays(GL_LINES, 0, lineCount * 4);
}
