#pragma once

#include <string>
#include <fstream>
#include <sstream>

// macros inspired by StackOverflow:
// https://stackoverflow.com/a/8338089

#define LOG_INFO(message)	\
	Log::info(static_cast<std::ostringstream&>(std::ostringstream().flush() << message).str())

#define LOG_ERROR(message)	\
	Log::error(static_cast<std::ostringstream&>(std::ostringstream().flush() << message).str())

class Log
{
private:
	static const std::string LOG_FILENAME;

	static std::ofstream file;

	static void output(std::string message);

public:
	Log();
	~Log();

	static void open();									// attempt to open the file
	static void close();								// attempt to close the file

	// while you can use these methods below, it is preferable to use the LOG_XXXX() macros defined above instead,
	// since they're more flexible and easier to use

	static void info(std::string message);				// log regular info, no new line
	static void error(std::string message);				// log error that quits the program
};
