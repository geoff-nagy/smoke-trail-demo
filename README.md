# Smoke Trail Demo

A small OpenGL program that demonstrates how to render lit smoke trails. This effect is physically plausible, if not physically realistic. It approximates ambient, diffuse, and back lighting (i.e., bleed-through of light along outer edges of smoke).

Built for Windows only, but there's no reason it couldn't be easily compiled for Linux or another operating system (although anything that uses GLES, like the Raspberry Pi, would require some minor modifications).

## Running

Both the debug and release executables are included in the root directory. Use the mouse (click-and-drag) to rotate, and the mouse wheel to zoom.

## Compiling

Executable is already included, but if you want to make modifications and build it, you'll need Code::Blocks. If you don't have it already, you can download Code::Blocks from [here](http://codeblocks.org/downloads/26).

## Contact

Feel free to contact Geoff at geoff.nagy@gmail.com if you have any questions.