#version 150

uniform mat4 u_Projection;		// camera projection (e.g., orthographic, perspective)
uniform mat4 u_Model;			// how this vertex should be transformed
uniform mat4 u_View;			// position and orientation of camera

in vec3 a_Vertex;				// incoming position of vertex

void main()
{
	// matrix transformations are always evaluated in reverse order, so the "vertex" vector
	// multiplication here is done first, which is what we want, and so on...
	gl_Position = u_Projection * u_View * u_Model* vec4(a_Vertex, 1.0);
}
